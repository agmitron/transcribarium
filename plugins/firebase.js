import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyCyhWtmWbm3SGEFhU7uOiLRNIEd1y9gBnY",
    authDomain: "vocalmatic-aaed8.firebaseapp.com",
    databaseURL: "https://vocalmatic-aaed8.firebaseio.com",
    projectId: "vocalmatic-aaed8",
    storageBucket: "vocalmatic-aaed8.appspot.com",
    messagingSenderId: "405316652039",
    appId: "1:405316652039:web:b8f57d827ec061e7a2eea2",
    measurementId: "G-FFDR9RYDJB"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);