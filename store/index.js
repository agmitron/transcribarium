import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import messagePlugin from '@/plugins/message.plugin.js'
import transcriptions from './transcriptions'
import settings from './settings'

Vue.use(Vuex)
Vue.use(messagePlugin)
const store = () => new Vuex.Store({
    state: {
        error: null, 
    },
    mutations: {
        setError(state, error) {
            state.error = error
        }, 
        clearError(state) {
            state.error = null
        }
    },
    actions: {

    }, 
    getters: {
        error: s => s.error
    }, 
    modules: {
        auth,
        transcriptions, 
        settings
    }
})

export default store