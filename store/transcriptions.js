import firebase from 'firebase'

export default {
    actions: {
        loadFile({ commit }, { file, name }) {
            commit('loadFile', { file, name })
            console.log(firebase.auth())
        },
        addFileToQueue({ commit }, file) {
            commit('addFileToQueue', file)
        },
        deleteFileFromQueue({ commit }, file) {
            commit('deleteFileFromQueue', file)
        },
        updateBarWidth({ commit }, { id, counter }) {
            commit('updateBarWidth', { id, counter })
        }
    },
    state: {
        loadedFile: {},
        loadingFiles: []
    },
    getters: {},
    mutations: {
        loadFile(state, { file, name }) {
            state.loadedFile = { file, name }
        },
        addFileToQueue(state, file) {
            state.loadingFiles.push(file)
        },
        deleteFileFromQueue(state) {
            state.loadingFiles.pop()
        },
        updateBarWidth(state, { id, counter }) {
            const neededFile = state.loadingFiles.find(lf => lf.currentID === id)
            neededFile.barWidth = counter * 100 / (neededFile.audioDuration * 2)
        }
    }
}