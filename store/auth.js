import firebase from 'firebase'

export default {
    state: {
        isAuth: false
    },
    actions: {
        async login({ dispatch, commit }, { email, password }) {
            try {
                await firebase.auth().signInWithEmailAndPassword(email, password)
                dispatch('setAuth')
            } catch (e) {
                commit('setError', e)
                throw e
            }
        },
        async registration({ dispatch }, { email, password, name }) {
            try {
                await firebase.auth().createUserWithEmailAndPassword(email, password)
                const uid = await dispatch('getUid')
                console.log(uid)
                await firebase.database().ref(`/users/${uid}/info`).set({
                    email, 
                    name
                })
            } catch (e) {
                commit('setError', e)
                throw e
            }
        },
        getUid() {
            const user = firebase.auth().currentUser
            return user ? user.uid : null
        },
        async updateEmail({ dispatch }, email) {
            try {
                const user = firebase.auth().currentUser
                await user.updateEmail(email).then(() => { console.log(`update good to ${email} was succesful`) })
            } catch (e) { }
        },
        setAuth({ commit }) {
            commit('setAuth')
        },
        async logout() {
            await firebase.auth().signOut()
        }
    },
    mutations: {
        setAuth(state) {
            state.isAuth = true
        }
    }
}