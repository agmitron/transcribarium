import firebase from 'firebase'

export default {
    state: {
        fullName: null,
        email: null, // 
        wasBorn: null
    },
    actions: {
        async setData({ commit, dispatch }) {
            try {
                const uid = await dispatch('getUid')
                const userData = (await dispatch('getData', uid)).info
                commit('setData', {
                    fullName: userData.name, 
                    wasBorn: userData.wasBorn, 
                    email: userData.email
                })
            } catch (e) {
                throw e
            }
        },
        async changeData({ commit, dispatch }, data) {
            try {
                const uid = await dispatch('getUid')
                await commit('setData', data)
                await firebase.database().ref(`users/${uid}/info`).set({
                    name: data.fullName,
                    wasBorn: data.wasBorn
                })
                const totalData = await commit('setData', data)
                console.log('totalData: ', this.state)
            } catch (e) {
                throw e
            }
        },
        getUid() {
            const user = firebase.auth().currentUser.uid
            return user ? user.uid : null
        },
        async getData({ commit, dispatch }, uid) {
            try {
                const data = await firebase.database().ref(`users`)
                    .once('value')

                const datas = data.val()
                return datas[uid]
            } catch (e) {
                throw e
            }
        },

    },
    mutations: {
        setData(state, { fullName, email, wasBorn }) {
            state.fullName = fullName
            state.wasBorn = wasBorn
            state.email = email
        }
    },
}