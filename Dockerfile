FROM node

ENV APP_ROOT /transcribarium

ENV DB_USER root

ENV DB_PASSWORD az905467

ENV DB_DATABASE transcribarium

RUN apt update

WORKDIR ${APP_ROOT}

ADD . ${APP_ROOT}

RUN apt install ffmpeg -y 

RUN npm i 

RUN npm run build 

RUN cp -r ./node_modules/m3u8stream/dist ./node_modules/m3u8stream/lib

CMD ["npm", "run", "start"]