const express = require('express')
const consola = require('consola')
const path = require('path')
const { Nuxt, Builder } = require('nuxt')
const request = require('request-promise')
const multer = require('multer')
const app = express()
const setAsrKey = require('./asr')
const fs = require('fs')
const db = require('./Database');
const bodyParser = require('body-parser')
// const { Worker } = require('worker_threads');
const http = require('http')
const WebSocket = require('ws')
const { getAudioDurationInSeconds } = require('get-audio-duration')


const MediaSplit = require('media-split');


const asyncRecognizer = async ({ splitPath, array, counter, asr, onPush, onLastSplitRecognized, splitsQuantity, condition }) => {
  const buffer = fs.readFileSync(splitPath)
  const result = await asr(buffer)
  array[splitsQuantity - counter] = result
  onPush(result)

  if (!array.includes(undefined)) {
    console.log('onLastSplitRecognized')
    onLastSplitRecognized()
  }
}

const getSplitProps = async (file, lengthCounter) => {
  let currentCapacity = 0;
  let splitPath;
  let splitSize;
  await new Promise((resolve, reject) => {
    console.log('Promise started...')
    fs.stat(file.path, (err, stats) => {
      if (err) {
        console.error(err)
        return
      }
      resolve(stats)
    })
  })
    .then(async stats => {
      let seconds, minutes

      if (lengthCounter < 60) {
        seconds = lengthCounter
        minutes = `00`
      } else if (lengthCounter === 60) {
        seconds = `00`
        minutes = `01`
      } else if (lengthCounter > 60) {
        seconds = lengthCounter % 60
        minutes = Math.floor(lengthCounter / 60)
      }

      await new Promise(async (resolve, reject) => {
        console.log('Second Promise started...')
        console.log('lengthCounter: ', lengthCounter)
        split = new MediaSplit({
          input: `./static/uploads/${file.filename}`,
          sections: [`[${minutes}:${+seconds > 0 ? +seconds - 1 : +seconds} - ${minutes}:${+seconds + 9}] ${lengthCounter}-${file.filename}`],
          output: './static/audioOutput',
          format: 'mp3'
        })
        console.log('minutes: ', minutes)
        console.log('seconds: ', seconds)
        console.log('+seconds > 0 ? +seconds - 1 : +seconds: ', +seconds > 0 ? +seconds - 1 : +seconds)
        await split.parse().then(sections => {
          for (let section of sections) {
            console.log('section.name: ', section.name);      // filename
            console.log('section.start', section.start);     // section start
            console.log('section.end', section.end);       // section end
            console.log('section.trackName', section.trackName); // track name
            console.log('section: ', section)
          }
        })
        resolve(split)
      })
        .then(async lastSplit => {
          await new Promise((resolve, reject) => {
            console.log('Third Promise started...')
            splitPath = `${lastSplit._options.output}/${lengthCounter}-${file.filename}.mp3`
            console.log('lastSplit: ', lastSplit)
            fs.stat(`${lastSplit._options.output}/${lengthCounter}-${file.filename}.mp3`, (err, splitStat) => {
              if (err) {
                console.error(err)
                return
              }
              resolve(splitStat)
            })
          })
            .then(async splitStat => {
              splitSize = await splitStat.size
            })
        })
    })
  console.log(splitPath)
  console.log('Function finished...')
  const obj = {
    path: splitPath,
    size: splitSize
  }
  return obj
}



const storage = multer.diskStorage({
  destination: './static/uploads',
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
})

// app.use('/account/transcriptions', bodyParser.urlencoded({
//   extended: true
// }));

// Init Upload

const server = new WebSocket.Server({ port: 1289 })
const upload = multer({
  storage,
}).single('filedata')

app.post('/account/transcriptions', bodyParser.text(), (req, res) => {

  let asr;
  new Promise((resolve) => upload(req, res, err => {
    if (err) {
      console.error(err)
    } else {
      resolve(req.file)
    }
  })).then(async file => {
    let asrKey
    if (req.body.currentLanguage == 'RUS') {
      asrKey = 'UZT3L3WDG2ZVSOULLFBBX4CQYWXFXKDF'
    } else if (req.body.currentLanguage == 'ENG') {
      asrKey = 'B6H6BIWSWDGPNAY7FDLVWAGHLJPYPNQA'
    }
    console.log('file.path: ', file.path)

    asr = setAsrKey(asrKey)
    let audioDuration

    if (req.body.duration) {
      audioDuration = req.body.duration
    } else {
      await getAudioDurationInSeconds(file.path).then((duration) => {
        console.log('duration: ', duration)
        audioDuration = parseInt(duration)
      })
    }
    res.redirect('/account/transcriptions')

    new Promise((resolve, reject) => {
      let clients = new Set()
      server.once('connection', ws => {
        const currentID = Date.now()
        const splits = []
        let results = []
        console.log('connected!!!!!!!!!!!!!!!!!')
        if (ws.readyState === ws.OPEN) {
          new Promise(async (resolve, reject) => {

            ws.send(JSON.stringify({
              type: 'fileData',
              filename: file.originalname,
              audioDuration,
              currentID
            }))
            for (let i = 0; i < audioDuration; i += 10) {
              if (ws.bufferedAmount === 0)
                ws.send(JSON.stringify({
                  counter: i,
                  currentID
                }))
              let everySplit = await getSplitProps(file, i)
              splits.push(everySplit)
              if (everySplit.size < 2048) {
                if (ws.bufferedAmount === 0)
                  ws.send(JSON.stringify({
                    counter: i,
                    currentID
                  }))
                splits.push(everySplit)
                break
              }
            }
            results = new Array(splits.length)
            resolve(splits)
          }).then(async splits => {
            console.log('splits: ', splits)
            for (let i = 0; i < splits.length; ++i) {
              if (ws.bufferedAmount === 0)
                ws.send(JSON.stringify({
                  counter: (i + splits.length - 1) * 10,
                  currentID
                }))


              asyncRecognizer({
                splitPath: splits[i].path,
                array: results,
                counter: i,
                onPush: split => {
                  console.log('pushed split: ', split)
                },
                asr,
                onLastSplitRecognized: () => {
                  console.log('Last split was recognized!!!')
                  ws.close()
                },
                splitsQuantity: splits.length - 1,
                condition: results.length == splits.length - 1
              })

            }
            if (!results) res.end('Incorrect audio type')
          })
        }

        if (ws.readyState === ws.CLOSING) {
          console.log('closing...')
        }

        ws.onclose = () => {
          console.log('closing')
          console.log('ws closed from server')
          results = results.reverse()
          console.log('results: ', results)
          db.query(`INSERT INTO words(words, originalSpeech, path, speechName, speechDescription, currentUserID, filename, dateOfLoad, isFile) VALUES ("${results.join(' ')}","${results.join(' ')}", "${file.filename}", "${req.body.speechName}", "${req.body.speechDescription}", "${req.body.currentUser}", "${file.originalname}", "${parseInt(req.body.dateOfLoad)}", "${req.body.isFile}")`)
          res.end()
        }
      })
    })
  })
})

app.post('/account/get_transcriptions', (req, res) => {
  db.query('SELECT * FROM `words`', function (err, result, fields) {
    return res.json(JSON.stringify(result));
  });
});

app.post('/account/transcriptions/get_original_speech', bodyParser.text(), (req, res) => {
  const id = req.body
  db.query(`SELECT originalSpeech from words WHERE id=${id}`, (err, result) => res.json(JSON.stringify(result)))
})

app.post('/account/transcriptions/save_and_update', bodyParser.text(), (req, res) => {
  const { speechDescription, words, speechName, prevWords, beforePrevWords, id } = JSON.parse(req.body)
  db.query(`UPDATE words SET speechDescription="${speechDescription}", words="${words}", speechName="${speechName}", prevWords="${prevWords}", beforePrevWords="${beforePrevWords}" WHERE id=${id}`)
})

app.post('/account/get_loading_data', (req, res) => {
  db.query('SELECT  * from `loading`', function (err, result, fields) {
    return res.json(JSON.stringify(result));
  });
});

app.delete('/account/transcriptions/delete', bodyParser.text(), (req, res) => {
  const id = req.body
  db.query(`DELETE FROM words WHERE id=${id}`, (err, result) => res.json(JSON.stringify(result)))
})



// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
