const request = require('request-promise')

const uri = `https://api.wit.ai/speech`
const apikey = 'B6H6BIWSWDGPNAY7FDLVWAGHLJPYPNQA' // получаем ключ доступа на wit.ai

module.exports = token =>
  async (body) => {
    // отправляем post запрос с буфером аудио
    const response = await request.post({
      uri,
      headers: {
        'Accept': 'audio/x-mpeg-3',
        'Authorization': `Bearer ` + token,
        'Content-Type': 'audio/mpeg3',
        'Transfer-Encoding': 'chunked'
      },
      body
    })
    // парсим ответ и отдаем результат расшифровки
    return JSON.parse(response)._text
  }
