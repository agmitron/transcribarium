const mysql = require('mysql')
var pool = mysql.createPool({
    connectionLimit: 100,
    host: 'localhost',
    user: 'root',
    password: 'az905467', 
    database: 'transcribarium', 
})

console.log('env: ', process.env ? process.env : false)
console.log(process.env.DB_USER ? process.env.DB_USER : false)
console.log(process.env.DB_PASSWORD ? process.env.DB_PASSWORD : false)
console.log(process.env.DB_DATABASE ? process.env.DB_DATABASE : false)

pool.getConnection((err, connection) => {
    if (err) {
        console.error(err)
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }
    if (connection) {
        console.log('connected')
        connection.release()
    }
    return
})
module.exports = pool

// const mysql2 = require('mysql2')

// const connection = mysql2.createConnection({
//     host: "192.168.64.2", 
//     user: "root", 
//     database: "voc", 
//     password: ""
// })

// connection.connect(err => {
//     if (err) {
//         console.error(err.message)
//     } else {
//         console.log('Success')
//     }
// })

// connection.end(err => {
//     if (err) {
//         console.error(err)
//     } else {
//         console.log('End')
//     }
// })